import React, { Component, Fragment } from 'react';
import {connect} from "react-redux";
import { PageHeader } from "react-bootstrap";
import PostForm from "../../components/PostForm/PostForm";
import {createPost} from "../../store/actions/news";



class NewPost extends Component {
  createPost = post => {
    this.props.onPostCreated(post).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <PageHeader>New post</PageHeader>
        <PostForm onSubmin={this.createPost} />
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onPostCreated: post => dispatch(createPost(post))
  }
};

export default connect(null, mapDispatchToProps)(NewPost);