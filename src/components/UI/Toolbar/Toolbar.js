import React from 'react';
import {Navbar} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";

const Toolbar = () => (
  <Navbar>
    <Navbar.Header>
      <Navbar.Brand>
        <LinkContainer to="/" exact><a>News</a></LinkContainer>
      </Navbar.Brand>
    </Navbar.Header>
  </Navbar>
);

export default Toolbar;