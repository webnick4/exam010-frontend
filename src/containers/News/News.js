import React, { Component, Fragment } from 'react';
import {connect} from "react-redux";
import {Button, Image, PageHeader, Panel} from "react-bootstrap";
import {Link} from "react-router-dom";
import {fetchNews} from "../../store/actions/news";


class News extends Component {
  componentDidMount() {
    this.props.onFetchNews();
  }

  render() {
    return (
      <Fragment>
        <PageHeader>
          Posts
          <Link to="/news/new">
            <Button bsStyle="primary" className="pull-right">Add post</Button>
          </Link>
        </PageHeader>

        {console.log(this.props.posts)}
        {this.props.posts.map(post => (
          <Panel key={post.id}>
            <Panel.Body>
              { post.image &&
              <Image
                style={{width: '100px', marginRight: '10px', float: 'left'}}
                src={'http://localhost:8000/uploads/' + post.image}
                thumbnail
              />
              }

              <strong>{post.title}</strong>
              <p>{post.post_date}</p>

              <Link to={'/news/' + post.id}>
                <span>Read full post >></span>
              </Link>
              <Link to={'/news/' + post.id}>
                <span className="pull-right">Delete</span>
              </Link>
            </Panel.Body>
          </Panel>
        ))}

      </Fragment>
    )
  }
}

const mapStateToProps = state => {
  return {
    posts: state.news.posts
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchNews: () => dispatch(fetchNews())
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(News);